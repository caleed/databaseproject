﻿using System;
using System.Collections.Generic;
using System.Text;
using Entities.Models;
using Microsoft.EntityFrameworkCore;

namespace Entities
{
    public class RepositoryContext : DbContext
    {
        public RepositoryContext(DbContextOptions options) : base(options) { }

        // model as table in database
        public DbSet<Admin> Admin { get; set; }

        public DbSet<Mecz> Mecz { get; set; }

        public DbSet<Zawodnik> Zawodnik { get; set; }

        public DbSet<Trener> Trener { get; set; }

        public DbSet<Klub> Klub { get; set; }


    }
}