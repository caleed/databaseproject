﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Models
{
    [Table("zawodnik")]
    public class Zawodnik
    {
        [Key]
        [Column("idZawodnik")]
        public int IdZawodnik { get; set; }

        [Column("Imie")]
        [StringLength(45, ErrorMessage = "Imie nie moze byc dluzsze 45 characters")]
        public string Imie { get; set; }

        [Column("Nazwisko")]
        [StringLength(45, ErrorMessage = "Nazwisko nie moze byc dluzsze 45 characters")]
        public string Nazwisko { get; set; }

        [Column("Uchwyt")]
        [StringLength(45, ErrorMessage = "Uchwyt nie moze byc dluzsze 2 characters")]
        public string Uchwyt { get; set; }

        [Column("Pozycja")]
        [StringLength(45, ErrorMessage = "Pozycja nie moze byc dluzsze 3 characters")]
        public string Pozycja { get; set; }

        [Column("Pochodzenie")]
        [StringLength(45, ErrorMessage = "Pochodzenie nie moze byc dluzsze 45 characters")]
        public string Pochodzenie { get; set; }

        [Column("Klub_Nazwa")]
        [StringLength(45, ErrorMessage = "Klubnie moze byc dluzsze 70 characters")]
        public string KlubNazwa { get; set; }



    }
}
