﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Models
{
    [Table("admin")]
    public class Admin
    {
        [Key]
        [Column("idAdmin")]
        public int Id { get; set; }

        [Column("login")]
        [StringLength(45, ErrorMessage = "Login nie moze byc dluzsze 45 znakow")]
        public string Login { get; set; }

        [Column("haslo")]
        [StringLength(45, ErrorMessage = "Haslo nie moze byc dluzsze 45 znakow")]
        public string Haslo { get; set; }
    }
}
