﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Models
{
    [Table("mecz")]
    public class Mecz
    {
        [Key]
        [Column("idMecz")]
        public int Id { get; set; }

        [Column("Gole_gospodarza")]
        public int GoleGospodarza { get; set; }

        [Column("Gole_goscia")]
        public int GoleGoscia { get; set; }

        [Column("Gospodarz")]
        [StringLength(70, ErrorMessage = "Gospodarz nie moze byc dluzsze 70 znakow")]
        public string Gospodarz { get; set; }

        [Column("Gosc")]
        [StringLength(70, ErrorMessage = "Gosc nie moze byc dluzsze 45 znakow")]
        public string Gosc { get; set; }


    }
}
