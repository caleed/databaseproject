﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Models
{
    [Table("klub")]
    public class Klub
    {
        [Key]
        [Column("Nazwa")]
        [StringLength(70, ErrorMessage = "Nazwa nie moze byc dluzsze 70 characters")]
        public string Nazwa { get; set; }

        [Column("Kraj")]
        [StringLength(45, ErrorMessage = "Kraj nie moze byc dluzsze 45 characters")]
        public string Kraj { get; set; }

        [Column("Strzelone_gole")]
        public int StrzeloneGole { get; set; }

        [Column("Stracone_Gole")]
        public int StraconeGole { get; set; }

        [Column("Trener_idTrener")]
        public int TrenerId { get; set; }

    }
}
