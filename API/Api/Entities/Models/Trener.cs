﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Models
{
    [Table("trener")]
    public class Trener
    {
        [Key]
        [Column("idTrener")]
        public int IdTrener { get; set; }

        [Column("Imie")]
        [StringLength(20, ErrorMessage = "Imie nie moze byc dluzsze 20 znakow")]
        public string Imie { get; set; }

        [Column("Nazwisko")]
        [StringLength(45, ErrorMessage = "Nazwisko nie moze byc dluzsze 45 znakow")]
        public string Nazwisko { get; set; }

        [Column("Pochodzenie")]
        [StringLength(45, ErrorMessage = "Pochodzenie nie moze byc dluzsze 45 znakow")]
        public string Pochodzenie { get; set; }


    }
}

