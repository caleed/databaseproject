﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;

namespace Contracts
{
    public interface IKlubRepository : IRepositoryBase<Klub>
    {
        IEnumerable<Klub> GetAllKlubs();
        Klub GetKlubById(string tytul);
        Klub[] GetTabelaKlubById(string tytul);
        void CreateKlub(Klub klub);
        void UpdateKlub(Klub dbKlub, Klub klub);
    }
}
