﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contracts
{
    public interface IRepositoryWrapper
    {
        IAdminRepository Admin { get; }
        IKlubRepository Klub { get; }
        ITrenerRepository Trener { get; }
        IMeczRepository Mecz { get; }
        IZawodnikRepository Zawodnik { get; }
        void Save();
    }
}
