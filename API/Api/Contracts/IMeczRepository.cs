﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;

namespace Contracts
{
    public interface IMeczRepository : IRepositoryBase<Mecz>
    {
        IEnumerable<Mecz> GetAllMeczs();
        Mecz GetmeczById(int id);
        Mecz[] GetMeczByGosc(string tytul);
        Mecz[] GetMeczByGospodarz(string tytul);
        void CreateMecz(Mecz mecz);
        void UpdateMecz(Mecz dbmecz, Mecz nowyMecz);
    }
}
