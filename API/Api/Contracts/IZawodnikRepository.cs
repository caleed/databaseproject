﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Entities.Models;

namespace Contracts
{
    public interface IZawodnikRepository : IRepositoryBase<Zawodnik>
    {
        IEnumerable<Zawodnik> GetAllZawodniks();
        Zawodnik[] GetZawodnikByNazwisko(string tytul);
        Zawodnik[] GetZawodnikByPozycja(string tytul);
        Zawodnik[] GetZawodnikByKlub(string tytul);
        Zawodnik GetZawodnikById(int id);
        void CreateZawodnik(Zawodnik zawodnik);
        void UpdateZawodnik(Zawodnik dbzawodnik, Zawodnik zawodnik);
    }
}
