﻿using System;
using System.Collections.Generic;
using System.Text;
using Entities;
using Entities.Models;
using Contracts;
using System.Linq;

namespace Repository
{
    public class MeczRepository : RepositoryBase<Mecz>, IMeczRepository
    {
        public MeczRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }

        public IEnumerable<Mecz> GetAllMeczs()
        {
            return FindAll()
                .OrderBy(ow => ow.Id).DefaultIfEmpty(new Mecz())
                .ToList();
        }

        public Mecz[] GetMeczByGosc(string tytul)
        {
            return FindByCondition(mecz => mecz.Gosc.Equals(tytul)).DefaultIfEmpty(new Mecz())
           .ToArray();
        }

        public Mecz[] GetMeczByGospodarz(string tytul)
        {
            return FindByCondition(mecz => mecz.Gospodarz.Equals(tytul)).DefaultIfEmpty(new Mecz())
           .ToArray();
        }

        public void CreateMecz(Mecz mecz)
        {
            Create(mecz);
        }

        public Mecz GetmeczById(int id)
        {
            return FindByCondition(mecz => mecz.Id.Equals(id))
                    .FirstOrDefault();
        }

        public void UpdateMecz(Mecz dbmecz, Mecz nowyMecz)
        {
            dbmecz.Gospodarz = nowyMecz.Gospodarz;
            dbmecz.Gosc = nowyMecz.Gosc;
            dbmecz.GoleGospodarza = nowyMecz.GoleGospodarza;
            dbmecz.GoleGoscia = nowyMecz.GoleGoscia;
            Update(dbmecz);
        }
    }
}