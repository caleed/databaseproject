﻿using System;
using System.Collections.Generic;
using System.Text;
using Entities;
using Entities.Models;
using Contracts;
using System.Linq;

namespace Repository
{
    public class AdminRepository : RepositoryBase<Admin>, IAdminRepository
    {
        public AdminRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }

        public IEnumerable<Admin> GetAllAdmins()
        {
            return FindAll()
                .OrderBy(ow => ow.Id).DefaultIfEmpty(new Admin())
                .ToList();
        }
    }
}
