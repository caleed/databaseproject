﻿using System;
using System.Collections.Generic;
using System.Text;
using Entities;
using Entities.Models;
using Contracts;
using System.Linq;

namespace Repository
{
    public class TrenerRepository : RepositoryBase<Trener>, ITrenerRepository
    {
        public TrenerRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }

        public IEnumerable<Trener> GetAllTreners()
        {
            return FindAll()
                .OrderBy(ow => ow.IdTrener).DefaultIfEmpty(new Trener())
                .ToList();
        }

        public void CreateTrener(Trener trener)
        {          
            Create(trener);
        }
    }
}
