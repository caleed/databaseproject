﻿using System;
using System.Collections.Generic;
using System.Text;
using Entities;
using Entities.Models;
using Contracts;
using System.Linq;

namespace Repository
{
    public class KlubRepository : RepositoryBase<Klub>, IKlubRepository
    {
        public KlubRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }

        public IEnumerable<Klub> GetAllKlubs()
        {
            return FindAll()
                .OrderBy(ow => ow.Nazwa).DefaultIfEmpty(new Klub())
                .ToList();
        }

        public Klub GetKlubById(string tytul)
        {
            return FindByCondition(klub => klub.Nazwa.Equals(tytul)).DefaultIfEmpty(new Klub())
           .FirstOrDefault();
        }

        public Klub[] GetTabelaKlubById(string tytul)
        {
            return FindByCondition(klub => klub.Nazwa.Equals(tytul)).DefaultIfEmpty(new Klub())
           .ToArray();
        }

        public void CreateKlub(Klub klub)
        {
            Create(klub);
        }

        public void UpdateKlub(Klub dbKlub, Klub klub)
        {
            dbKlub.Nazwa = klub.Nazwa;
            dbKlub.Kraj = klub.Kraj;
            dbKlub.StrzeloneGole = klub.StrzeloneGole;
            dbKlub.StraconeGole = klub.StraconeGole;
            dbKlub.TrenerId = klub.TrenerId;
            Update(dbKlub);
        }


    }
}
