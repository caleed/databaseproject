﻿using Entities;
using Contracts;

namespace Repository
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private RepositoryContext _repoContext;
        private IAdminRepository _admin;
        private ITrenerRepository _trener;
        private IMeczRepository _mecz;
        private IZawodnikRepository _zawodnik;
        private IKlubRepository _klub;

        public IAdminRepository Admin
        {
            get
            {
                if (_admin == null)
                {
                    _admin = new AdminRepository(_repoContext);
                }

                return _admin;
            }
        }

        public ITrenerRepository Trener
        {
            get
            {
                if (_trener == null)
                {
                    _trener = new TrenerRepository(_repoContext);
                }

                return _trener;
            }
        }

        public IMeczRepository Mecz
        {
            get
            {
                if (_mecz == null)
                {
                    _mecz = new MeczRepository(_repoContext);
                }

                return _mecz;
            }
        }

        public IKlubRepository Klub
        {
            get
            {
                if (_klub == null)
                {
                    _klub = new KlubRepository(_repoContext);
                }

                return _klub;
            }
        }

        public IZawodnikRepository Zawodnik
        {
            get
            {
                if (_zawodnik == null)
                {
                    _zawodnik = new ZawodnikRepository(_repoContext);
                }

                return _zawodnik;
            }
        }

        public RepositoryWrapper(RepositoryContext repositoryContext)
        {
            _repoContext = repositoryContext;
        }

        public void Save()
        {
            _repoContext.SaveChanges();
        }
    }
}
