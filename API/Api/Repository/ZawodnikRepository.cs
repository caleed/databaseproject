﻿using System;
using System.Collections.Generic;
using System.Text;
using Entities;
using Entities.Models;
using Contracts;
using System.Linq;

namespace Repository
{
    public class ZawodnikRepository : RepositoryBase<Zawodnik>, IZawodnikRepository
    {
        public ZawodnikRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }

        public IEnumerable<Zawodnik> GetAllZawodniks()
        {
            return FindAll()
                .OrderBy(ow => ow.IdZawodnik).DefaultIfEmpty(new Zawodnik())
                .ToList();
        }
        
        public Zawodnik[] GetZawodnikByNazwisko(string tytul)
        {
            return FindByCondition(zawodnik => zawodnik.Nazwisko.Equals(tytul)).DefaultIfEmpty(new Zawodnik())
                .ToArray();
        }

        public Zawodnik[] GetZawodnikByPozycja(string tytul)
        {
            return FindByCondition(zawodnik => zawodnik.Pozycja.Equals(tytul)).DefaultIfEmpty(new Zawodnik())
           .ToArray();
          

        }

        public Zawodnik[] GetZawodnikByKlub(string tytul)
        {
            return FindByCondition(zawodnik => zawodnik.KlubNazwa.Equals(tytul)).DefaultIfEmpty(new Zawodnik()).ToArray();
        }

        public void CreateZawodnik(Zawodnik Zawodnik)
        {
            Create(Zawodnik);
            
        }

        public void UpdateZawodnik(Zawodnik dbzawodnik, Zawodnik zawodnik)
        {
            dbzawodnik.Imie = zawodnik.Imie;
            dbzawodnik.Nazwisko = zawodnik.Nazwisko;
            dbzawodnik.Uchwyt = zawodnik.Uchwyt;
            dbzawodnik.Pozycja = zawodnik.Pozycja;
            dbzawodnik.Pochodzenie = zawodnik.Pochodzenie;
            dbzawodnik.KlubNazwa = zawodnik.KlubNazwa;
            Update(dbzawodnik);
        }

        public Zawodnik GetZawodnikById(int id)
        {
            return FindByCondition(owner => owner.IdZawodnik.Equals(id))
                    .FirstOrDefault();
        }

    }
}