﻿using Contracts;
using Entities.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Web;

namespace AccountOwnerServer.Controllers
{
    [Route("api/klub")]
    [ApiController]
    public class KlubController : ControllerBase
    {
        
        private IRepositoryWrapper _repository;

        public KlubController(IRepositoryWrapper repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public IActionResult GetAllKlubs()
        {
            try
            {
                var owners = _repository.Klub.GetAllKlubs();

                return Ok(owners);
            }
            catch (Exception)
            {     
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpGet("{title}", Name = "daj")]
        public IActionResult GetKlubById(string title)
        {           
            var tmp = title.Replace("%20", " ");
            try
            {               
                var klub = _repository.Klub.GetKlubById(HttpUtility.UrlDecode(title));

                if (klub == null)
                {
                    return NotFound();
                }
                else
                {
                    return Ok(klub);
                }
            }
            catch (Exception )
            {
                return StatusCode(500, "Internal server error");
            }
        }


        [HttpGet("kluby/{title}", Name = "inna")]
        public IActionResult GetTabelaKlubById(string title)
        {
            var tmp = title.Replace("%20", " ");
            try
            {
                var klub = _repository.Klub.GetTabelaKlubById(HttpUtility.UrlDecode(title));

                if (klub == null)
                {
                    return NotFound();
                }
                else
                {
                    return Ok(klub);
                }
            }
            catch (Exception)
            {
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPost(Name ="foo")]
        public IActionResult CreateKlub([FromBody]Klub klub)
        {
            try
            {
                if (klub == null)
                { 
                    return BadRequest("Owner object is null");
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest("Invalid model object");
                }

                _repository.Klub.CreateKlub(klub);
                _repository.Save();

                return CreatedAtRoute("foo", new { nazwa = klub.Nazwa }, klub);
            }
            catch (Exception ex)
            {

                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPut("zmien/{tytul}")]
        public IActionResult UpdateKlub(string tytul, [FromBody]Klub klub)
        {

            try
            {
        
                if (!ModelState.IsValid)
                {
                    return BadRequest("Invalid model object");
                }

                var dbKlub = _repository.Klub.GetKlubById(tytul);


                _repository.Klub.UpdateKlub(dbKlub, klub);
                _repository.Save();

                return NoContent();
            }
            catch (Exception)
            {
                return StatusCode(500, "Internal server error");
            }
        }


    }
}