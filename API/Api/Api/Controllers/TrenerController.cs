﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using Entities.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AccountOwnerServer.Controllers
{
    [Route("api/trener")]
    [ApiController]
    public class TrenerController : ControllerBase
    {

        private IRepositoryWrapper _repository;

        public TrenerController(IRepositoryWrapper repository)
        {
            _repository = repository;
        }

        [HttpGet(Name ="OwnerById")]
        public IActionResult GetAllTreners()
        {
            try
            {
                var trener = _repository.Trener.GetAllTreners();

                return Ok(trener);
            }
            catch (Exception)
            {
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPost]
        public IActionResult CreateTrener([FromBody]Trener trener)
        {
            try
            {
                if (trener == null)
                {
                    return BadRequest("Owner object is null");
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest("Invalid model object");
                }

                _repository.Trener.CreateTrener(trener);
                _repository.Save();

                return CreatedAtRoute("OwnerById", new { id = trener.IdTrener }, trener);
            }
            catch (Exception)
            {
                return StatusCode(500, "Internal server error");
            }
        }

    }
}
