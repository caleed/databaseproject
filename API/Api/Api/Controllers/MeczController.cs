﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Contracts;
using Entities.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AccountOwnerServer.Controllers
{
    [Route("api/mecz")]
    [ApiController]
    public class MeczController : ControllerBase
    {

        private IRepositoryWrapper _repository;

        public MeczController(IRepositoryWrapper repository)
        {
            _repository = repository;
        }

        [HttpGet(Name ="MeczById")]
        public IActionResult GetAllMeczs()
        {
            try
            {
                var mecz = _repository.Mecz.GetAllMeczs();

                return Ok(mecz);
            }
            catch (Exception)
            {
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpGet("{id}")]
        public IActionResult GetMeczById(int id)
        {
            try
            {
                var owner = _repository.Mecz.GetmeczById(id);

                if (owner == null)
                {
                    return NotFound();
                }
                else
                {
                    return Ok(owner);
                }
            }
            catch (Exception)
            {
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpGet("gosc/{title}")]
        public IActionResult GetKlubByGosc(string title)
        {
            var tmp = title.Replace("%20", " ");
            try
            {
                var mecz = _repository.Mecz.GetMeczByGosc(HttpUtility.UrlDecode(title));

               // var mecz2 = _repository.Mecz.GetMeczByGospodarz(HttpUtility.UrlDecode(title));

                if (mecz == null)
                {
                    return NotFound();
                }
                else
                {
                    return Ok(mecz);
                }
            }
            catch (Exception)
            {
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpGet("gospodarz/{tytul}")]
        public IActionResult GetKlubByGospodarz(string tytul)
        {
            var tmp = tytul.Replace("%20", " ");
            try
            {
                var mecz = _repository.Mecz.GetMeczByGospodarz(HttpUtility.UrlDecode(tytul));

                if (mecz == null)
                {
                    return NotFound();
                }
                else
                {
                    return Ok(mecz);
                }
            }
            catch (Exception)
            {
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPost]
        public IActionResult CreateMecz([FromBody]Mecz mecz)
        {
            try
            {
                if (mecz == null)
                {
                    return BadRequest("Owner object is null");
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest("Invalid model object");
                }

                _repository.Mecz.CreateMecz(mecz);
                _repository.Save();

                return CreatedAtRoute("MeczById", new { id = mecz.Id }, mecz);
            }
            catch (Exception)
            {
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPut("{id}")]
        public IActionResult UpdateMecz(int id, [FromBody]Mecz mecz)
        {
            try
            {

                if (!ModelState.IsValid)
                {
                    return BadRequest("Invalid model object");
                }

                var dbMecz = _repository.Mecz.GetmeczById(id);
             
                _repository.Mecz.UpdateMecz(dbMecz, mecz);
                _repository.Save();

                return NoContent();
            }
            catch (Exception)
            {
                return StatusCode(500, "Internal server error");
            }
        }


    }
}
