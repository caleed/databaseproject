﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Contracts;
using Entities.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AccountOwnerServer.Controllers
{
    [Route("api/zawodnik")]
    [ApiController]
    public class ZawodnikController : ControllerBase
    {

        private IRepositoryWrapper _repository;

        public ZawodnikController(IRepositoryWrapper repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public IActionResult GetAllZawodniks()
        {
            try
            {
                var zawodnik = _repository.Zawodnik.GetAllZawodniks();

                return Ok(zawodnik);
            }
            catch (Exception)
            {
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpGet("{id}")]
        public IActionResult GetZawodnikById(int id)
        {
            try
            {
                var zawodnik = _repository.Zawodnik.GetZawodnikById(id);

                if (zawodnik == null)
                {
                    return NotFound();
                }
                else
                {
                    return Ok(zawodnik);
                }
            }
            catch (Exception)
            {;
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpGet("bynazwisko/{nazwisko}", Name ="ZawodnikById")]
        public IActionResult GetZawodnikByNazwisko(string nazwisko)
        {
           // var tmp = title.Replace("%20", " ");
            try
            {
                var zawodnik = _repository.Zawodnik.GetZawodnikByNazwisko(HttpUtility.UrlDecode(nazwisko));

                if (zawodnik == null)
                {
                    return NotFound();
                }
                else
                {
                    return Ok(zawodnik);
                }
            }
            catch (Exception)
            {
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpGet("bypozycja/{title}")]
        public IActionResult GetZawodnikByPozycja(string title)
        {
            var tmp = title.Replace("%20", " ");
            try
            {
                var zawodnik = _repository.Zawodnik.GetZawodnikByPozycja(HttpUtility.UrlDecode(title));

                if (zawodnik == null)
                {
                    return NotFound();
                }
                else
                {
                    return Ok(zawodnik);
                }
            }
            catch (Exception)
            {
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpGet("byklub/{title}")]
        public IActionResult GetZawodnikByKlub(string title)
        {
            var tmp = title.Replace("%20", " ");
            try
            {
                var zawodnik = _repository.Zawodnik.GetZawodnikByKlub(HttpUtility.UrlDecode(title));

                if (zawodnik == null)
                {
                    return NotFound();
                }
                else
                {
                    return Ok(zawodnik);
                }
            }
            catch (Exception)
            {
                return StatusCode(500, "Internal server error");
            }
        }


        [HttpPost]
        public IActionResult CreateZawodnik([FromBody]Zawodnik zawodnik)
        {
            try
            {
                if (zawodnik == null)
                {
                    return BadRequest("Owner object is null");
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest("Invalid model object");
                }

                _repository.Zawodnik.CreateZawodnik(zawodnik);
                _repository.Save();

                return CreatedAtRoute("MeczByid", new { id = zawodnik.IdZawodnik }, zawodnik);
            }
            catch (Exception)
            {
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPut("{id}")]
        public IActionResult UpdateZawodnik(int id, [FromBody]Zawodnik zawodnik)
        {
            try
            {

                if (!ModelState.IsValid)
                {
                    return BadRequest("Invalid model object");
                }

                var dbZawodnik = _repository.Zawodnik.GetZawodnikById(id);

                _repository.Zawodnik.UpdateZawodnik(dbZawodnik, zawodnik);
                _repository.Save();

                return NoContent();
            }
            catch (Exception )
            {
                return StatusCode(500, "Internal server error");
            }
        }

    }
}
