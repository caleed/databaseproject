﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Klub
    {
        public string Nazwa { get; set; }

        public string Kraj { get; set; }

        public int StrzeloneGole { get; set; }
        
        public int StraconeGole { get; set; }

        public int TrenerId { get; set; }
    }
}
