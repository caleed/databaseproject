﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Mecz
    {
        public int Id { set; get; }

        public string Gospodarz { set; get; }

        public string Gosc { get; set; }

        public int GoleGospodarza { get; set; }

        public int GoleGoscia { get; set; }

    }
}
