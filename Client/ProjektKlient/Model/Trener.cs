﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Trener
    {
        public int IdTrener { get; set; }

        public string Imie { get; set; }

        public string Nazwisko { get; set; }

        public string Pochodzenie { get; set; }

        
    }
}
