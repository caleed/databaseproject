﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Model
{
    public class Zawodnik
    {
        public int IdZawodnik { get; set; }

        public string Imie { get; set; }

        public string Nazwisko { get; set; }

        public string Uchwyt { get; set; }

        public string Pozycja { get; set; }

        public string Pochodzenie { get; set; }

        public string KlubNazwa { get; set; }

    }
}
