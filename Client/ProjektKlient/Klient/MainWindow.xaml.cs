﻿using Demo;
using Klient.viewmodel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klient
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            ApiHelper.InitializeClient();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Content = new HomeViewModel();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

            Content = new AdminLoginModel();
          //  Content = new AdminHomeViewModel();
        }
       
    }
}
