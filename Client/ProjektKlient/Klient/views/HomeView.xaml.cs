﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Klient.viewmodel;

namespace Klient.views
{
    /// <summary>
    /// Logika interakcji dla klasy HomeView.xaml
    /// </summary>
    public partial class HomeView : UserControl
    {
        public HomeView()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Content = new MeczeViewModel();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Content = new ZawodnicyViewModel();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            Content = new TrenerzyViewModel();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            Content = new KlubyViewModel();
        }
    }
}
