﻿using Demo;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Model;
using Klient.viewmodel;

namespace Klient.views
{
    /// <summary>
    /// Logika interakcji dla klasy AdminLogin.xaml
    /// </summary>
    public partial class AdminLoginView : UserControl
    {
        private string log;
        private string has;

        public AdminLoginView()
        {
            InitializeComponent();
        }

        public static async Task<Admin[]> LoadAdmin()
        {
            try
            {
                HttpResponseMessage response = await ApiHelper.ApiClient.GetAsync("http://localhost:5000/api/admin");

                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsStringAsync();
                    Admin[] jsonObject = JsonConvert.DeserializeObject<Admin[]>(result);

                    List<Admin> res = new List<Admin>();
                    foreach (Admin ev in jsonObject)
                    {
                        res.Add(ev);
                        Console.WriteLine(ev);
                    }
                    return res.ToArray();
                }
                else
                {
                    throw new Exception(response.StatusCode.ToString());
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        async private void Button_Click(object sender, RoutedEventArgs e)
        {
            log = Konto.Text;
            has = Pasy.Password.ToString();
            Admin[] adm = await LoadAdmin();
            Admin ad = adm[0];

            if(ad.Login == log && ad.Haslo == has)
            {
                Content = new AdminHomeViewModel();
            }
        }
    }
}
