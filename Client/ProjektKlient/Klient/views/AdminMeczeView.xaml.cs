﻿using Demo;
using Klient.viewmodel;
using Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klient.views
{
    /// <summary>
    /// Logika interakcji dla klasy AdminMeczeView.xaml
    /// </summary>
    public partial class AdminMeczeView : UserControl
    {
        private string wyszukaj { get; set; }
        private string gospoda { get; set; }
        private string gos { get; set; }
        private string goleGospodarz { get; set; }
        private string goleGos { get; set; }

        public AdminMeczeView()
        {
            InitializeComponent();
            Load();
        }

        public static async Task<Mecz[]> LoadDoWyszukaniaGospodarz(string tytul)
        {
            try
            {
                string link = "http://localhost:5000/api/mecz/gospodarz/" + tytul;
                HttpResponseMessage response = await ApiHelper.ApiClient.GetAsync(link);
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsStringAsync();

                    Mecz[] jsonObject = JsonConvert.DeserializeObject<Mecz[]>(result);

                    List<Mecz> res = new List<Mecz>();
                    foreach (Mecz ev in jsonObject)
                    {
                        res.Add(ev);
                    }

                    return res.ToArray();

                }
                else
                {
                    throw new Exception(response.StatusCode.ToString());
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static async Task<Mecz[]> LoadDoWyszukaniaGosc(string tytul)
        {
            try
            {
                string link = "http://localhost:5000/api/mecz/gosc/" + tytul;
                HttpResponseMessage response = await ApiHelper.ApiClient.GetAsync(link);

                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsStringAsync();

                    Mecz[] jsonObject = JsonConvert.DeserializeObject<Mecz[]>(result);

                    List<Mecz> res = new List<Mecz>();
                    foreach (Mecz ev in jsonObject)
                    {
                        res.Add(ev);
                    }

                    return res.ToArray();

                }
                else
                {
                    throw new Exception(response.StatusCode.ToString());
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async void LoadWyszukiwarke()
        {
            try
            {
                wyszukaj = wyszukiwarka.Text;
                DataGrid1.Columns.Clear();
                DataGrid1.ItemsSource = null;
                Mecz[] current = await LoadDoWyszukaniaGosc(wyszukaj);
                Mecz[] current2 = await LoadDoWyszukaniaGospodarz(wyszukaj);
                Mecz[] wynik = current.Concat(current2).ToArray();
                DataGrid1.ItemsSource = wynik;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        public static async Task<Mecz[]> LoadMecze()
        {
            try
            {
                HttpResponseMessage response = await ApiHelper.ApiClient.GetAsync("http://localhost:5000/api/mecz");

                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsStringAsync();
                    Mecz[] jsonObject = JsonConvert.DeserializeObject<Mecz[]>(result);

                    List<Mecz> res = new List<Mecz>();
                    foreach (Mecz ev in jsonObject)
                    {
                        res.Add(ev);
                        Console.WriteLine(ev);
                    }
                    return res.ToArray();

                }
                else
                {
                    throw new Exception(response.StatusCode.ToString());
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void HomeBtn_Clicked(object sender, RoutedEventArgs e)
        {
            Content = new AdminHomeViewModel();
        }

        public async void Load()
        {
            try
            {
                Mecz[] current = await LoadMecze();
                DataGrid1.ItemsSource = current;

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }


        private void SzukajBtn_Clicked(object sender, RoutedEventArgs e)
        {
            wyszukaj = wyszukiwarka.Text;
            if (string.IsNullOrEmpty(wyszukaj))
            {
                Load();
            }
            else
            {
                LoadWyszukiwarke();
            }

        }

        public static async Task<bool> AddMecz(string gospodarz, string gosc, string golegospodarza, string golegoscia)
        {
            object data = new
            {
                gospodarz,
                gosc,
                golegospodarza,
                golegoscia
            };
            var json = JsonConvert.SerializeObject(data);
            var content = new StringContent(json.ToString(), Encoding.UTF8, "application/json");
            try
            {
                HttpResponseMessage response = await ApiHelper.ApiClient.PostAsync("http://localhost:5000/api/mecz/", content);
                int code = (int)response.StatusCode;
                if (code >= 200 && code < 300)
                {
                    return true;
                }
                else
                {
                    throw new Exception(response.StatusCode.ToString());
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        async public void Dodaj()
        {
            gospoda = Gospo.Text;
            gos = Gos.Text;
            goleGospodarz = GGospo.Text;
            goleGos = GGos.Text;
            await AddMecz(gospoda, gos, goleGospodarz, goleGos);
            DataGrid1.Columns.Clear();
            DataGrid1.ItemsSource = null;
            Load();
        }

        private void DodajBtn_Clicked(object sender, RoutedEventArgs e)
        {
            Dodaj();
        }

        public static async Task<bool> UpdateEvent(int Id, string Gospodarz, string Gosc, int GoleGospodarza, int GoleGoscia)
        {
            object obj = new
            {
                Id,
                Gospodarz,
                Gosc,
                GoleGospodarza,
                GoleGoscia
            };
            var json = JsonConvert.SerializeObject(obj);
            var content = new StringContent(json.ToString(), Encoding.UTF8, "application/json");
            try
            {
                HttpResponseMessage response = await ApiHelper.ApiClient.PutAsync("http://localhost:5000/api/mecz/" + Id, content);
                int code = (int)response.StatusCode;
                if (code >= 200 && code < 300)
                {
                    return true;
                }
                else
                {
                    throw new Exception(response.StatusCode.ToString());
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        async private void ModBtn_Clicked(object sender, RoutedEventArgs e)
        {
            int klucz = Convert.ToInt32(IdModBtn.Text);
            string gosposia = GospodarzModBtn.Text;
            string goscia = GoscModBtn.Text;
            int GolGospo = Convert.ToInt32(GGospodarzModBtn.Text);
            int GolGosposia = Convert.ToInt32(GGoscModBtn.Text);
            await UpdateEvent(klucz, gosposia, goscia, GolGospo, GolGosposia);
            Load();
        }
    }
}
