﻿using Demo;
using Klient.viewmodel;
using Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klient.views
{
    /// <summary>
    /// Logika interakcji dla klasy TrenerzyView.xaml
    /// </summary>
    public partial class TrenerzyView : UserControl
    {
        public TrenerzyView()
        {
            InitializeComponent();
            Load();
        }

        private void HomeBtn_Clicked(object sender, RoutedEventArgs e)
        {
            Content = new HomeViewModel();
        }

        public static async Task<Trener[]> LoadTrenerzy()
        {
            try
            {
                HttpResponseMessage response = await ApiHelper.ApiClient.GetAsync("http://localhost:5000/api/trener");

                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsStringAsync();
                    Trener[] jsonObject = JsonConvert.DeserializeObject<Trener[]>(result);

                    List<Trener> res = new List<Trener>();
                    foreach (Trener ev in jsonObject)
                    {
                        res.Add(ev);
                    }
                    return res.ToArray();
                }
                else
                {
                    throw new Exception(response.StatusCode.ToString());
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async void Load()
        {
            try
            {
                Trener[] current = await LoadTrenerzy();
                DataGrid1.ItemsSource = current;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }
    }
}
