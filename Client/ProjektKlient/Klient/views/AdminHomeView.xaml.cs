﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klient.views
{
    /// <summary>
    /// Logika interakcji dla klasy AdminHomeView.xaml
    /// </summary>
    public partial class AdminHomeView : UserControl
    {
        public AdminHomeView()
        {
            InitializeComponent();
        }

        private void ZawodnicyBtn_Clicked(object sender, RoutedEventArgs e)
        {
            Content = new AdminZawodnicyView();
        }

        private void MeczBtn_Clicked(object sender, RoutedEventArgs e)
        {
            Content = new AdminMeczeView();
        }

        private void KlubyBtn_Clicked(object sender, RoutedEventArgs e)
        {
            Content = new AdminKlubyView();
        }

        private void TrenerzyBtn_Clicked(object sender, RoutedEventArgs e)
        {
            Content = new AdminTrenerzyView();
        }
    }
}
