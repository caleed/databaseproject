﻿using Demo;
using Klient.viewmodel;
using Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klient.views
{
    /// <summary>
    /// Logika interakcji dla klasy AdminKlubyView.xaml
    /// </summary>
    public partial class AdminKlubyView : UserControl
    {
        private string wyszukaj { get; set; }
        private string nazwa { get; set; }
        private string kraj { get; set; }
        private int strzelonegole { get; set; }
        private int straconegole { get; set; }
        private int idTrenera { get; set; }

        public AdminKlubyView()
        {
            InitializeComponent();
            Load();
        }

        private void HomeBtn_Clicked(object sender, RoutedEventArgs e)
        {
            Content = new AdminHomeViewModel();
        }

        public static async Task<Klub[]> LoadKluby()
        {
            try
            {
                HttpResponseMessage response = await ApiHelper.ApiClient.GetAsync("http://localhost:5000/api/klub");

                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsStringAsync();
                    Klub[] jsonObject = JsonConvert.DeserializeObject<Klub[]>(result);

                    List<Klub> res = new List<Klub>();
                    foreach (Klub ev in jsonObject)
                    {
                        res.Add(ev);
                        Console.WriteLine(ev);
                    }
                    return res.ToArray();
                }
                else
                {
                    throw new Exception(response.StatusCode.ToString());
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async void Load()
        {
            try
            {
                Klub[] current = await LoadKluby();
                DataGrid1.ItemsSource = current;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        public static async Task<Klub[]> LoadDoWyszukaniaKlub(string tytul)
        {
            try
            {
                var tmp = tytul.Replace(" ", "%20");
                string link = "http://localhost:5000/api/klub/kluby/" + tytul;
                HttpResponseMessage response = await ApiHelper.ApiClient.GetAsync(link);

                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsStringAsync();
                    Klub[] jsonObject = JsonConvert.DeserializeObject<Klub[]>(result);

                    List<Klub> res = new List<Klub>();
                    foreach (Klub ev in jsonObject)
                    {
                        res.Add(ev);
                    }

                    return res.ToArray();
                
                }
                else
                {
                    throw new Exception(response.StatusCode.ToString());
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async void LoadWyszukiwarke()
        {
            try
            {
                wyszukaj = wyszukiwarka.Text;
                DataGrid1.Columns.Clear();
                DataGrid1.ItemsSource = null;
                Klub[] current = await LoadDoWyszukaniaKlub(wyszukaj);
                DataGrid1.ItemsSource = current;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        private void SzukajBtn_Clicked(object sender, RoutedEventArgs e)
        {
            wyszukaj = wyszukiwarka.Text;
            if (string.IsNullOrEmpty(wyszukaj))
            {
                Load();
            }
            else
            {
                LoadWyszukiwarke();
            }

        }

        public static async Task<bool> AddZawodnik(string Nazwa, string Kraj, int StrzeloneGole, int StraconeGole, int TrenerId)
        {
            object data = new
            {
                Nazwa,
                Kraj,
                StrzeloneGole,
                StraconeGole,
                TrenerId
            };
            var json = JsonConvert.SerializeObject(data);
            var content = new StringContent(json.ToString(), Encoding.UTF8, "application/json");
            try
            {
                HttpResponseMessage response = await ApiHelper.ApiClient.PostAsync("http://localhost:5000/api/klub/", content);
                int code = (int)response.StatusCode;
                if (code >= 200 && code < 300)
                {
                    return true;
                }
                else
                {
                    throw new Exception(response.StatusCode.ToString());
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        async private void DodajBtn_Clicked(object sender, RoutedEventArgs e)
        {
            nazwa = NazwaKlubu.Text;
            kraj = KrajKlubu.Text;
            strzelonegole = Convert.ToInt32(StrzeloneGoleKlubu.Text);
            straconegole = Convert.ToInt32(StraconeGoleKlubu.Text);
            idTrenera = Convert.ToInt32(IdTreneraKlubu.Text);
            await AddZawodnik(nazwa, kraj, strzelonegole, straconegole, idTrenera);
            DataGrid1.Columns.Clear();
            DataGrid1.ItemsSource = null;
            Load();
        }

        public static async Task<bool> UpdateKlub(string Nazwa, string Kraj, int StrzeloneGole, int StraconeGole, int TrenerId)
        {          
            object obj = new
            {
                Nazwa,
                Kraj,
                StrzeloneGole,
                StraconeGole,
                TrenerId
            };
            var tmp = Nazwa.Replace(" ", "%20");
            var json = JsonConvert.SerializeObject(obj);
            var content = new StringContent(json.ToString(), Encoding.UTF8, "application/json");
            try
            {
                HttpResponseMessage response = await ApiHelper.ApiClient.PutAsync("http://localhost:5000/api/klub/zmien/" + Nazwa, content);
                int code = (int)response.StatusCode;
                if (code >= 200 && code < 300)
                {
                    return true;
                }
                else
                {
                    throw new Exception(response.StatusCode.ToString());
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        async private void ModBtn_Clicked(object sender, RoutedEventArgs e)
        {
            nazwa = NazwMod.Text;
            kraj = KrajMod.Text;
            strzelonegole = Convert.ToInt32(StrzeloneMod.Text);
            straconegole = Convert.ToInt32(StraconeMod.Text);
            idTrenera = Convert.ToInt32(TrenerIdMod.Text);
            await UpdateKlub(nazwa, kraj, strzelonegole, straconegole, idTrenera);
            Load();
        }
    }
}
