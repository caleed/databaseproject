﻿using Demo;
using Klient.viewmodel;
using Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klient.views
{
    /// <summary>
    /// Logika interakcji dla klasy AdminTrenerzyView.xaml
    /// </summary>
    public partial class AdminTrenerzyView : UserControl
    {
        private string imie { get; set; }
        private string nazwisko { get; set; }
        private string pochodzenie { get; set; }

        public AdminTrenerzyView()
        {
            InitializeComponent();
            Load();      
        }

        async private void DodajBtn_Clicked(object sender, RoutedEventArgs e)
        {
            imie = Im.Text;
            nazwisko = Nazw.Text;
            pochodzenie = Poch.Text;
            await AddTrener(imie, nazwisko,pochodzenie);
            Load();
        }

        public static async Task<bool> AddTrener(string imie, string nazwisko, string pochodzenie)
        {
            object data = new
            {
                imie,
                nazwisko,
                pochodzenie
            };
            var json = JsonConvert.SerializeObject(data);
            var content = new StringContent(json.ToString(), Encoding.UTF8, "application/json");
            try
            {
                HttpResponseMessage response = await ApiHelper.ApiClient.PostAsync("http://localhost:5000/api/trener/", content);
                int code = (int)response.StatusCode;
                if (code >= 200 && code < 300)
                {
                    return true;
                }
                else
                {
                    throw new Exception(response.StatusCode.ToString());
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static async Task<Trener[]> LoadTrenerzy()
        {
            try
            {
                HttpResponseMessage response = await ApiHelper.ApiClient.GetAsync("http://localhost:5000/api/trener");

                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsStringAsync();
                    Trener[] jsonObject = JsonConvert.DeserializeObject<Trener[]>(result);

                    List<Trener> res = new List<Trener>();
                    foreach (Trener ev in jsonObject)
                    {
                        res.Add(ev);
                    }
                    return res.ToArray();

                }
                else
                {
                    throw new Exception(response.StatusCode.ToString());
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async void Load()
        {
            try
            {
                Trener[] current = await LoadTrenerzy();
                DataGrid1.ItemsSource = current;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        private void HomeBtn_Clicked(object sender, RoutedEventArgs e)
        {
            Content = new AdminHomeViewModel();
        }
    }
}
