﻿using Demo;
using Klient.viewmodel;
using Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klient.views
{
    /// <summary>
    /// Logika interakcji dla klasy ZawodnicyView.xaml
    /// </summary>
    public partial class ZawodnicyView : UserControl
    {
        private string wyszukaj { get; set; }

        public ZawodnicyView()
        {
            InitializeComponent();
            Load();
        }

        private void HomeBtn_Clicked(object sender, RoutedEventArgs e)
        {
            Content = new HomeViewModel();
        }

        public static async Task<Zawodnik[]> LoadZawodnik()
        {
            try
            {
                HttpResponseMessage response = await ApiHelper.ApiClient.GetAsync("http://localhost:5000/api/zawodnik");

                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsStringAsync();
                    Zawodnik[] jsonObject = JsonConvert.DeserializeObject<Zawodnik[]>(result);

                    List<Zawodnik> res = new List<Zawodnik>();
                    foreach (Zawodnik ev in jsonObject)
                    {
                        res.Add(ev);
                        Console.WriteLine(ev);
                    }
                    return res.ToArray();
                }
                else
                {
                    throw new Exception(response.StatusCode.ToString());
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async void Load()
        {
            try
            {
                Zawodnik[] current = await LoadZawodnik();
                DataGrid1.ItemsSource = current;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        public static async Task<Zawodnik[]> LoadZawodnikNazwisko(string tytul)
        {
            try
            {               
                string link = "http://localhost:5000/api/zawodnik/bynazwisko/" + tytul;
                HttpResponseMessage response = await ApiHelper.ApiClient.GetAsync(link);

                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsStringAsync();
                    Zawodnik[] jsonObject = JsonConvert.DeserializeObject<Zawodnik[]>(result);

                    List<Zawodnik> res = new List<Zawodnik>();
                    foreach (Zawodnik ev in jsonObject)
                    {
                        res.Add(ev);
                    }

                    return res.ToArray();
                }
                else
                {
                    throw new Exception(response.StatusCode.ToString());
                }
            }           
            catch (Exception e)
            {
                throw e;
            }
        }

        public static async Task<Zawodnik[]> LoadZawodnikPozycja(string tytul)
        {
            try
            {
                string link = "http://localhost:5000/api/zawodnik/bypozycja/" + tytul;
                HttpResponseMessage response = await ApiHelper.ApiClient.GetAsync(link);

                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsStringAsync();

                    Zawodnik[] jsonObject = JsonConvert.DeserializeObject<Zawodnik[]>(result);

                    List<Zawodnik> res = new List<Zawodnik>();
                    foreach (Zawodnik ev in jsonObject)
                    {
                        res.Add(ev);
                    }

                    return res.ToArray();

                }
                else
                {
                    throw new Exception(response.StatusCode.ToString());
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static async Task<Zawodnik[]> LoadZawodnikKlub(string tytul)
        {
            try
            {
                var tmp = tytul.Replace(" ", "%20");
                string link = "http://localhost:5000/api/zawodnik/byklub/" + tytul;
                HttpResponseMessage response = await ApiHelper.ApiClient.GetAsync(link);

                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsStringAsync();

                    Zawodnik[] jsonObject = JsonConvert.DeserializeObject<Zawodnik[]>(result);

                    List<Zawodnik> res = new List<Zawodnik>();
                    foreach (Zawodnik ev in jsonObject)
                    {
                        res.Add(ev);
                    }

                    return res.ToArray();

                }
                else
                {
                    throw new Exception(response.StatusCode.ToString());
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void SzukajBtn_Clicked(object sender, RoutedEventArgs e)
        {
            wyszukaj = wyszukiwarka.Text;
            if (string.IsNullOrEmpty(wyszukaj))
            {
                Load();
            }
            else
            {
                LoadWyszukiwarke();
            }

        }

        public async void LoadWyszukiwarke()
        {
            try
            {
                wyszukaj = wyszukiwarka.Text;
                DataGrid1.Columns.Clear();
                DataGrid1.ItemsSource = null;
                Zawodnik[] bynazwisko = await LoadZawodnikNazwisko(wyszukaj);
                Zawodnik[] byklub = await LoadZawodnikKlub(wyszukaj);
                Zawodnik[] bypozycja = await LoadZawodnikPozycja(wyszukaj);
                
                foreach(Zawodnik zaw in bynazwisko)
                {
                    if(wyszukaj == zaw.Nazwisko)
                    {
                        DataGrid1.ItemsSource = bynazwisko;
                    }
                }

                foreach (Zawodnik zaw in bypozycja)
                {
                    if (wyszukaj == zaw.Pozycja)
                    {
                        DataGrid1.ItemsSource = bypozycja;
                    }
                }

                foreach (Zawodnik zaw in byklub)
                {
                    if (wyszukaj == zaw.KlubNazwa)
                    {
                        DataGrid1.ItemsSource = byklub;
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

    }
}
