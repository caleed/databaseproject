﻿using Demo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Model;
using Newtonsoft.Json;
using Klient.viewmodel;

namespace Klient.views
{
    /// <summary>
    /// Logika interakcji dla klasy MeczView.xaml
    /// </summary>
    public partial class MeczView : UserControl
    {
        private string wyszukaj { get; set; }

        public MeczView()
        {
            InitializeComponent();
            Load();
        }

        public static async Task<Mecz[]> LoadDoWyszukaniaGospodarz(string tytul)
        {
            try
            {
                string link = "http://localhost:5000/api/mecz/gospodarz/" + tytul;
                HttpResponseMessage response = await ApiHelper.ApiClient.GetAsync(link);
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsStringAsync();
                    Mecz[] jsonObject = JsonConvert.DeserializeObject<Mecz[]>(result);

                    List<Mecz> res = new List<Mecz>();
                    foreach (Mecz ev in jsonObject)
                    {
                        res.Add(ev);
                    }
                    return res.ToArray();
                }
                else
                {
                    throw new Exception(response.StatusCode.ToString());
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static async Task<Mecz[]> LoadDoWyszukaniaGosc(string tytul)
        {
            try
            {
                string link = "http://localhost:5000/api/mecz/gosc/" + tytul;
                HttpResponseMessage response = await ApiHelper.ApiClient.GetAsync(link);

                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsStringAsync();
                    Mecz[] jsonObject = JsonConvert.DeserializeObject<Mecz[]>(result);

                    List<Mecz> res = new List<Mecz>();
                    foreach (Mecz ev in jsonObject)
                    {
                        res.Add(ev);
                    }

                    return res.ToArray();
                }               
                else
                {
                    throw new Exception(response.StatusCode.ToString());
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async void LoadWyszukiwarke()
        {
            try
            {               
                wyszukaj = wyszukiwarka.Text;
                DataGrid1.Columns.Clear();
                DataGrid1.ItemsSource = null;
                Mecz[] current = await LoadDoWyszukaniaGosc(wyszukaj);
                Mecz[] current2 = await LoadDoWyszukaniaGospodarz(wyszukaj);
                Mecz[] wynik = current.Concat(current2).ToArray();
                DataGrid1.ItemsSource = wynik;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        public static async Task<Mecz[]> LoadMecze()
        {
            try
            {
                HttpResponseMessage response = await ApiHelper.ApiClient.GetAsync("http://localhost:5000/api/mecz");
                
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsStringAsync();
                    Mecz[] jsonObject = JsonConvert.DeserializeObject<Mecz[]>(result);

                    List<Mecz> res = new List<Mecz>();
                    foreach (Mecz ev in jsonObject)
                    {
                        res.Add(ev);
                        Console.WriteLine(ev);
                    }
                    return res.ToArray();
                }
                else
                {
                    throw new Exception(response.StatusCode.ToString());
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void HomeBtn_Clicked(object sender, RoutedEventArgs e)
        {
            Content = new HomeViewModel();
        }

        public async void Load()
        {
            try
            {
                Mecz[] current = await LoadMecze();
                 DataGrid1.ItemsSource = current;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        private void SzukajBtn_Clicked(object sender, RoutedEventArgs e)
        {
            wyszukaj = wyszukiwarka.Text;
            if (string.IsNullOrEmpty(wyszukaj))
            {
                Load();
            }
            else
            {
                LoadWyszukiwarke();
            }            
        }
    }
}
