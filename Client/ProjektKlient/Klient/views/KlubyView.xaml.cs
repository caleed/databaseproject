﻿using Demo;
using Klient.viewmodel;
using Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Klient.views
{
    /// <summary>
    /// Logika interakcji dla klasy KlubyView.xaml
    /// </summary>
    public partial class KlubyView : UserControl
    {
        private string wyszukaj { get; set; }

        public KlubyView()
        {
            InitializeComponent();
            Load();
        }

        private void HomeBtn_Clicked(object sender, RoutedEventArgs e)
        {
            Content = new HomeViewModel();
        }

        public static async Task<Klub[]> LoadKluby()
        {
            try
            {
                HttpResponseMessage response = await ApiHelper.ApiClient.GetAsync("http://localhost:5000/api/klub");

                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsStringAsync();
                    Klub[] jsonObject = JsonConvert.DeserializeObject<Klub[]>(result);

                    List<Klub> res = new List<Klub>();
                    foreach (Klub ev in jsonObject)
                    {
                        res.Add(ev);
                        Console.WriteLine(ev);
                    }
                    return res.ToArray();
                }
                else
                {
                    throw new Exception(response.StatusCode.ToString());
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async void Load()
        {
            try
            {
                Klub[] current = await LoadKluby();
                DataGrid1.ItemsSource = current;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        public static async Task<Klub[]> LoadDoWyszukaniaKlub(string tytul)
        {
            try
            {
                var tmp = tytul.Replace(" ", "%20");
                string link = "http://localhost:5000/api/klub/kluby/" + tytul;
                HttpResponseMessage response = await ApiHelper.ApiClient.GetAsync(link);

                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsStringAsync();

                    Klub[] jsonObject = JsonConvert.DeserializeObject<Klub[]>(result);

                    List<Klub> res = new List<Klub>();
                    foreach (Klub ev in jsonObject)
                    {
                        res.Add(ev);
                    }
                    
                    return res.ToArray();
                }
                else
                {
                    throw new Exception(response.StatusCode.ToString());
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async void LoadWyszukiwarke()
        {
            try
            {
                wyszukaj = wyszukiwarka.Text;
                DataGrid1.Columns.Clear();
                DataGrid1.ItemsSource = null;
                Klub[] current = await LoadDoWyszukaniaKlub(wyszukaj);            
                DataGrid1.ItemsSource = current;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        private void SzukajBtn_Clicked(object sender, RoutedEventArgs e)
        {
            wyszukaj = wyszukiwarka.Text;
            if (string.IsNullOrEmpty(wyszukaj))
            {
                Load();
            }
            else
            {
                LoadWyszukiwarke();
            }
        }
    }
}
